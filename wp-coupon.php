<?php 
/*
Plugin Name: WP Coupon Marketing
Plugin URI: mailto:jjfattz@gmail.com
Description: WP Coupon Marketing is a plugin that allows you to create coupons for your Wordpress site. It also allows you to add these coupons dynamically to content pages within your site in a controlled way using "short-codes". You can determine how many times a coupon will display along with the ability to dynamically re-direct users to secondary pages if they don't "win" the chance to view the coupon.
Author: Justin F.
Version: 1.1.0
Author URI: mailto:jjfattz@gmail.com
*/

/*  Copyright 2013  Justin F.  (email : jjfattz@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo "Hi there!  I'm just a plugin, not much I can do when called directly.";
	exit;
}

// GLOBALS
global $wpdb;
// Set and save plugin version info
if (!defined('WP_COUPON_VERSION_KEY'))
    define('WP_COUPON_VERSION_KEY', 'be62813815334020eeb89d33fedf4f9c');

if (!defined('WP_COUPON_VERSION_NUM'))
    define('WP_COUPON_VERSION_NUM', '1.1.0');

add_option(WP_COUPON_VERSION_KEY, WP_COUPON_VERSION_NUM);

if (!defined('WP_COUPON_ADMIN_SLUG'))
    define('WP_COUPON_ADMIN_SLUG', 'wp-coupon-settings');

if (!defined('WP_COUPON_PLUGIN_NAME'))
    define('WP_COUPON_PLUGIN_NAME', trim(dirname(plugin_basename(__FILE__)), '/'));

if (!defined('WP_COUPON_PLUGIN_DIR'))
    define('WP_COUPON_PLUGIN_DIR', WP_PLUGIN_DIR . '/' . WP_COUPON_PLUGIN_NAME);

if (!defined('WP_COUPON_PLUGIN_URL'))
    define('WP_COUPON_PLUGIN_URL', WP_PLUGIN_URL . '/' . WP_COUPON_PLUGIN_NAME);

if (!defined('WP_COUPON_DB_NAME'))
    define('WP_COUPON_DB_NAME', $wpdb->prefix.'wpcoupon_unique');

if (!defined('WP_COUPONE_ENTRY_DB'))
    define('WP_COUPONE_ENTRY_DB', $wpdb->prefix.'wpcoupon_entry_unique');

/** Step 2 (from text above). */
add_action('admin_menu', 'wp_coupon_menu');

// setup database
function wp_coupon_install_db() {
   global $wpdb;

   $table_name = WP_COUPON_DB_NAME;
   $entry_table_name = WP_COUPONE_ENTRY_DB;
      
	$sql = "CREATE TABLE $table_name (
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	stamp datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
	name VARCHAR(254) NOT NULL,
	advertiser VARCHAR(254) NOT NULL,
	code VARCHAR(254) NOT NULL,
	available INT(11) NOT NULL,
	method ENUM('First to visit', 'Random', 'Everyone wins') NOT NULL,
	winners INT(11) NOT NULL,
	txt_msg TEXT NOT NULL,
	txt_from VARCHAR(254) NOT NULL,
	email_to VARCHAR(254) NOT NULL,
	email_subject VARCHAR(254) NOT NULL,
	email_msg TEXT NOT NULL,
	email_from VARCHAR(254) NOT NULL,
	UNIQUE KEY id (id)
	);";

	$entry_sql = "CREATE TABLE $entry_table_name (
	id mediumint(9) NOT NULL AUTO_INCREMENT,
	stamp datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
	fname VARCHAR(254) NOT NULL,
	lname VARCHAR(254) NOT NULL,
	email VARCHAR(254) NOT NULL,
	phone VARCHAR(254) NOT NULL,
	carrier VARCHAR(254) NOT NULL,
	coupon_id mediumint(9) NOT NULL,
	UNIQUE KEY id (id)
	);";

   require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
   dbDelta($sql);
   dbDelta($entry_sql);
}

// initialize db
register_activation_hook( __FILE__, 'wp_coupon_install_db' );

function wp_coupon_you_lost($url) {
	?>
	<script type="text/javascript">window.location = "<?php echo $url; ?>";</script>
	<?php
	exit();
}

function wp_coupon_you_won($coupon_id) {
	global $wpdb;
	$wpcoupondb = WP_COUPON_DB_NAME;

	$coupon = $wpdb->get_results("SELECT * FROM $wpcoupondb WHERE id = $coupon_id LIMIT 1", ARRAY_A);
	$coupon = $coupon[0];
	$coupon_winners = intval($coupon['winners']) + 1;

	$wpdb->update(
		$wpcoupondb, 
		array(
			'winners' => $coupon_winners
		), 
		array( 'id' => $coupon_id )
	);

	return $coupon['code'];
}

// shortcut code/function to award coupons
function wp_coupon_run($atts) {
	$lose_url = $atts['lose_url'];
	$coupon_id = $atts['coupon_id'];

	global $wpdb;
	$wpcoupondb = WP_COUPON_DB_NAME;

	$coupon = $wpdb->get_results("SELECT * FROM $wpcoupondb WHERE id = $coupon_id LIMIT 1", ARRAY_A);
	$coupon = $coupon[0];

	if($coupon['winners'] < $coupon['available'] && $coupon['method'] != 'Everyone wins') {
		if($coupon['method'] == 'First to visit') {
			return wp_coupon_you_won($coupon_id);
		} elseif($coupon['method'] == 'Random') {
			$result = Rand (0, 1);
			if($result) {
				return wp_coupon_you_won($coupon_id);
			} else {
				return wp_coupon_you_lost($lose_url);
			}
		}
	} elseif($coupon['method'] == 'Everyone wins') {
		return wp_coupon_you_won($coupon_id);
	} else {
		return wp_coupon_you_lost($lose_url);
	}
}

// boolean function to award coupons
function wp_coupon_run_bool($lose_url, $coupon_id) {
	global $wpdb;
	$wpcoupondb = WP_COUPON_DB_NAME;

	$coupon = $wpdb->get_results("SELECT * FROM $wpcoupondb WHERE id = $coupon_id LIMIT 1", ARRAY_A);
	$coupon = $coupon[0];

	if($coupon['winners'] < $coupon['available'] && $coupon['method'] != 'Everyone wins') {
		if($coupon['method'] == 'First to visit') {
			return true;
		} elseif($coupon['method'] == 'Random') {
			$result = Rand (0, 1);
			if($result) {
				return true;
			} else {
				return false;
			}
		}
	} elseif($coupon['method'] == 'Everyone wins') {
		return true;
	} else {
		return false;
	}
}

// add the shortcut to use on frontend
add_shortcode( 'get_coupon', 'wp_coupon_run' );

function send_text($coupon_id, $phone, $carrier) {
	require_once('email_to_sms.php');
	global $wpdb;
	$wpcoupondb = WP_COUPON_DB_NAME;

	$coupon = $wpdb->get_results("SELECT * FROM $wpcoupondb WHERE id = $coupon_id LIMIT 1", ARRAY_A);
	$coupon = $coupon[0];

	$sms = new email_to_sms();
	$msg = $coupon['txt_msg'];
	$from = $coupon['txt_from'];
	return $sms->send($msg, $phone, $carrier, $from);
}

function send_email($coupon_id) {
	global $wpdb;
	$wpcoupondb = WP_COUPON_DB_NAME;

	$coupon = $wpdb->get_results("SELECT * FROM $wpcoupondb WHERE id = $coupon_id LIMIT 1", ARRAY_A);
	$coupon = $coupon[0];

	$to = $coupon['email_to'];
	$subject = $coupon['email_subject'];
	$msg = $coupon['email_msg'];
	$from = $coupon['email_from'];
	$headers = "from:$from\n";
	//using php mail() function
	//return mail($to, $subject, $msg, $headers);

	//using Wordpress wp-mail() function
	return wp_mail( $to, $subject, $msg, $headers );
}

// shortcut code to display default coupon form
function get_coupon_with_form($atts, $content) {
	$lose_url = $atts['lose_url'];
	$coupon_id = $atts['coupon_id'];

	if(isset($_POST['wp-coupon-form'])) {
		if(wp_coupon_run_bool($lose_url, $coupon_id)) {
			global $wpdb;
			$wp_entry_db = WP_COUPONE_ENTRY_DB;

			// insert new entry
			$wpdb->insert(
				$wp_entry_db, 
				array(
					'stamp' => date("Y-m-d H:i:s"),
					'fname' => $_POST['fname'],
					'lname' => $_POST['lname'],
					'email' => $_POST['email'],
					'phone' => $_POST['phone'],
					'carrier' => $_POST['carrier'],
					'coupon_id' => $coupon_id
				)
			);

			send_email($coupon_id);
			send_text($coupon_id, $_POST['phone'], $_POST['carrier']);
			return do_shortcode($content);
		} else {
			return wp_coupon_you_lost($lose_url);
		}

	} else {
	?>
	<style>
	.wpc-signup-form{padding:0 20px;background-color:#eee;}
	.wpc-form-row{height:90px;padding:15px 0;border-top:1px solid #fff;border-bottom:1px solid #ccc;display:block;}
	.wpc-signup-form input, .wpc-signup-form select{width:100%;}
	.wpc-form-chk{width:30px !important;}
	.wpc-form-row-first{border-top:0;}
	.wpc-form-row-last{border-bottom:0;}
	</style>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$("#signup-form").submit(function(e) {
			var isFilledOut = true;
			$("#signup-form input, #signup-form select").each(function(index) {
				if($(this).attr('type') == 'checkbox') {
					if(!$(this).is(':checked')) {
						isFilledOut = false;
					}
				} else if($(this).val() == '' || $(this).val() == null) {
					isFilledOut = false;
				}
			});

			if(!isFilledOut) {
				alert('All Fields Required.  Please make sure you fill out all fields and accept the terms and conditions to proceed');
				e.preventDefault();
				return false;
			}
		});
	});
	</script>
	<div class="wpc-signup-form">
		<form action="" method="post" id="signup-form">
			<input type="hidden" name="wp-coupon-form" value="true" />
			<div class="wpc-form-row wpc-form-row-first">
				<label for="fname">First Name</label><br />
				<input type="text" name="fname" class="wpc-form-txt" />
			</div>
			<div class="wpc-form-row">
				<label for="lname">Last Name</label><br />
				<input type="text" name="lname" class="wpc-form-txt" />
			</div>
			<div class="wpc-form-row">
				<label for="email">Email</label><br />
				<input type="text" name="email" class="wpc-form-txt" />
			</div>
			<div class="wpc-form-row">
				<label for="phone">Phone Number</label><br />
				<input type="text" name="phone" class="wpc-form-txt" />
			</div>
			<div class="wpc-form-row">
				<label for="carrier">Your Mobile Carrier</label><br />
				<select name="carrier">
					<option value=""></option>
					<option value="AT&T">AT&T</option>
					<option value="Boost Mobile">Boost Mobile</option>
					<option value="MetroPCS">Metro PCS</option>
					<option value="Qwest Wireless">Qwest Wireless</option>
					<option value="Sprint (PCS)">Sprint</option>
					<option value="T-Mobile">T-Mobile</option>
					<option value="US Cellular">US Cellular</option>
					<option value="Verizon">Verizon</option>
					<option value="Viaero">Viaero</option>
					<option value="Virgin Mobile">Virgin Mobile</option>
	 			</select>
			</div>
			<div class="wpc-form-row">
				<label for="email">Terms and Conditions</label><br />
				<label><input type="checkbox" name="terms" class="wpc-form-chk" /> I agree to the terms and conditions below</label>
			</div>
			<div class="wpc-form-row wpc-form-row-last">
				<input type="submit" name="form-submit" value="Get Coupon" />
			</div>
		</form>
	</div>
	<?php
	}
}

// add the shortcut to use on frontend
add_shortcode( 'get_coupon_with_form', 'get_coupon_with_form' );

function get_form_coupon($atts) {
	$coupon_id = $atts['coupon_id'];

	return wp_coupon_you_won($coupon_id);
}

// add the shortcut to use on frontend
add_shortcode( 'get_form_coupon', 'get_form_coupon' );

/** Step 1. */
function wp_coupon_menu() {
	add_menu_page('WP Coupon', 'WP Coupon', 'manage_options', WP_COUPON_ADMIN_SLUG, 'wp_coupon_render_page', WP_COUPON_PLUGIN_URL.'/img/admin-icon.png');
}

// function to display main admin section
function wp_coupon_display_main_admin() {
	global $wpdb;
	$wpcoupondb = WP_COUPON_DB_NAME;
	$wp_entry_db = WP_COUPONE_ENTRY_DB;
	?>
	<style>
	th.center, td.center{text-align:center;}
	td a img{vertical-align:middle;margin-right:5px;}
	</style>
	<div class="wrap">
		<h2>WP Coupon Marketing</h2>
		<a href="?page=<?php echo WP_COUPON_ADMIN_SLUG ?>&new-coupon" class="button-primary" style="float:right;">Create New Coupon</a>
		<h3 style="margin-top:5px;">Coupon List</h3>
		<table class="widefat">
			<thead>
				<tr>
					<th>Coupon Name</th>
					<th>Advertiser</th>
					<th>Coupon Code</th>
					<th>Last Edited</th>
					<th class="center">Winners</th>
					<th class="center">Total Coupons</th>
					<th class="center">Coupon ID</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$coupons = $wpdb->get_results( "SELECT * FROM $wpcoupondb ORDER BY stamp DESC", ARRAY_A);
				if(count($coupons) > 0) {
				foreach($coupons as $coupon) {
				?>
				<tr>
					<td><a href="?page=<?php echo WP_COUPON_ADMIN_SLUG ?>&coupon=<?php echo $coupon['id']; ?>"><img src="<?php echo WP_COUPON_PLUGIN_URL ?>/img/coupon-16.png" /> <?php echo $coupon['name']; ?></a></td>
					<td><?php echo $coupon['advertiser']; ?></td>
					<td><?php echo $coupon['code']; ?></td>
					<td><?php echo $coupon['stamp']; ?></td>
					<td class="center"><?php echo $coupon['winners']; ?></td>
					<td class="center"><?php echo $coupon['available']; ?></td>
					<td class="center"><?php echo $coupon['id']; ?></td>
				</tr>
				<?php }
				} else { ?>
				<tr>
					<td colspan="7" style="text-align:center;">No Coupons Found</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<br /><br />
	<style>
	th.center, td.center{text-align:center;}
	td a img{vertical-align:middle;margin-right:5px;}
	</style>
	<div class="wrap">
		<a href="<?php echo $_SERVER['REQUEST_URI'] ?>&exportall" target="_blank" class="button-primary" style="float:right;">Export to CSV</a>
		<h3 style="margin-top:5px;">All Email Entries</h3>
		<table class="widefat">
			<thead>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Carrier</th>
					<th>Date Entered</th>
					<th class="center">Coupon ID</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$entries = $wpdb->get_results( "SELECT * FROM $wp_entry_db ORDER BY stamp DESC", ARRAY_A);
				if(count($entries) > 0) {
				foreach($entries as $entry) {
				?>
				<tr>
					<td><?php echo $entry['fname']; ?></td>
					<td><?php echo $entry['lname']; ?></td>
					<td><?php echo $entry['email']; ?></td>
					<td><?php echo $entry['phone']; ?></td>
					<td><?php echo $entry['carrier']; ?></td>
					<td><?php echo $entry['stamp']; ?></td>
					<td class="center"><?php echo $entry['coupon_id']; ?></td>
				</tr>
				<?php }
				} else { ?>
				<tr>
					<td colspan="7" style="text-align:center;">No Entries Found</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<?php
}

function wp_coupon_display_footer() {
	?>
	<style>
	.wpcp-footer{display:block;font-size:10px;text-align:center;font-style:italic;margin-top:20px;color:#999;}
	</style>
	<div class="wpcp-footer">WP Coupon Plugin - Version <?php echo WP_COUPON_VERSION_NUM; ?><br /><a href="?page=<?php echo WP_COUPON_ADMIN_SLUG ?>&section=getting-started">Getting Started</a></div>
	<?php 
}

function array2csv(array &$array)
{
   if (count($array) == 0) {
     return null;
   }
   ob_start();
   $df = fopen("php://output", 'w');
   fputcsv($df, array_keys(reset($array)));
   foreach ($array as $row) {
      fputcsv($df, $row);
   }
   fclose($df);
   return ob_get_clean();
}

function download_send_headers($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}


add_action('admin_init','wp_coupon_export_all_entries');

function wp_coupon_export_all_entries() {
	if(isset($_REQUEST['exportall'])) {
		global $wpdb;
		$wp_entry_db = WP_COUPONE_ENTRY_DB;
		download_send_headers("email-export-" . date("Y-m-d") . ".csv");
		$entries = $wpdb->get_results( "SELECT * FROM $wp_entry_db ORDER BY stamp DESC", ARRAY_A);
		echo array2csv($entries);
		exit();
	}
}

add_action('admin_init','wp_coupon_export_entries');

function wp_coupon_export_entries() {
	if(isset($_REQUEST['coupon']) && isset($_REQUEST['export'])) {
		global $wpdb;
		$wp_entry_db = WP_COUPONE_ENTRY_DB;

		$couponid = $_REQUEST['coupon'];
		download_send_headers("email-export-" . date("Y-m-d") . ".csv");
		$entries = $wpdb->get_results( "SELECT * FROM $wp_entry_db WHERE coupon_id = $couponid ORDER BY stamp DESC", ARRAY_A);
		echo array2csv($entries);
		exit();
	}
}

function wp_coupon_update_coupon() {
	global $wpdb;
	$wpcoupondb = WP_COUPON_DB_NAME;
	$wp_entry_db = WP_COUPONE_ENTRY_DB;

	$couponid = $_REQUEST['coupon'];

	if(isset($_REQUEST['delete'])) {
		$wpdb->delete($wpcoupondb, array( 'id' => $couponid ));
		?>
		<script type="text/javascript">window.location = "?page=<?php echo WP_COUPON_ADMIN_SLUG; ?>";</script>
		<?php
		exit();
	}

	if(isset($_POST['submit'])) {
		$wpdb->update(
			$wpcoupondb, 
			array(
				'stamp' => date("Y-m-d H:i:s"),
				'name' => $_POST['coupon-name'],
				'advertiser' => $_POST['coupon-advertiser'],
				'code' => $_POST['coupon-code'],
				'available' => $_POST['coupon-available'],
				'method' => $_POST['coupon-method'],
				'txt_msg' => $_POST['coupon-txt-msg'],
				'txt_from' => $_POST['coupon-txt-from'],
				'email_to' => $_POST['coupon-em-to'],
				'email_subject' => $_POST['coupon-em-subject'],
				'email_msg' => $_POST['coupon-em-msg'],
				'email_from' => $_POST['coupon-em-from']
			), 
			array( 'id' => $couponid )
		);

		?>
		<script type="text/javascript">window.location = "?page=<?php echo WP_COUPON_ADMIN_SLUG; ?>";</script>
		<?php
		exit();
	}

	$coupon = $wpdb->get_results( "SELECT * FROM $wpcoupondb WHERE id = $couponid LIMIT 1", ARRAY_A);

	if(count($coupon) > 0) {
	?>
	<style type="text/css">
	.wpcp-form{background-color:#eee;padding:20px;margin-right:20px;}
	.wpcp-row{display:block;margin:20px 0;border-bottom:1px solid #e0e0e0;}
	.wpcp-link{}
	.wpcp-del-link{color:#f00;text-decoration:underline;float:right;}
	.wpcp-row input, .wpcp-row select{margin-bottom:20px;}
	</style>
	<h2><?php echo $coupon[0]['name'] ?> - Coupon</h2>
	<p><a href="?page=<?php echo WP_COUPON_ADMIN_SLUG; ?>">Back to Admin</a></p>
	<div class="wpcp-form">
		<h3>General Settings</h3>
		<form name="" id="" action="" method="post">
		<div class="wpcp-row">
			<label>Coupon Name:</label><br />
			<input type="text" name="coupon-name" value="<?php echo $coupon[0]['name'] ?>" />
		</div>
		<div class="wpcp-row">
			<label>Advertiser:</label><br />
			<input type="text" name="coupon-advertiser" value="<?php echo $coupon[0]['advertiser'] ?>" />
		</div>
		<div class="wpcp-row">
			<label>Coupon Code:</label><br />
			<input type="text" name="coupon-code" value="<?php echo $coupon[0]['code'] ?>" />
		</div>
		<div class="wpcp-row">
			<label>Total Coupons to Win:</label><br />
			<input type="text" name="coupon-available" value="<?php echo $coupon[0]['available'] ?>" />
		</div>
		<div class="wpcp-row">
			<label>Winning Method:</label><br />
			<select name="coupon-method">
				<option value="First to visit"<?php if($coupon[0]['method']=='First to visit'){echo ' selected';} ?>>First to visit</option>
				<option value="Random"<?php if($coupon[0]['method']=='Random'){echo ' selected';} ?>>Random</option>
				<option value="Everyone wins"<?php if($coupon[0]['method']=='Everyone wins'){echo ' selected';} ?>>Everyone Wins (overrides total coupons)</option>
			</select>
		</div>
		<h3>Contact Settings</h3>
		<div class="wpcp-row">
			<label>Text Message:</label><br />
			<textarea name="coupon-txt-msg"><?php echo $coupon[0]['txt_msg'] ?></textarea>
		</div>
		<div class="wpcp-row">
			<label>Text From (your email address):</label><br />
			<input type="text" name="coupon-txt-from" value="<?php echo $coupon[0]['txt_from'] ?>" />
		</div>
		<div class="wpcp-row">
			<label>Email To (email address of advertiser):</label><br />
			<input type="text" name="coupon-em-to" value="<?php echo $coupon[0]['email_to'] ?>" />
		</div>
		<div class="wpcp-row">
			<label>Email Subject:</label><br />
			<input type="text" name="coupon-em-subject" value="<?php echo $coupon[0]['email_subject'] ?>" />
		</div>
		<div class="wpcp-row">
			<label>Email From (your email address):</label><br />
			<input type="text" name="coupon-em-from" value="<?php echo $coupon[0]['email_from'] ?>" />
		</div>
		<div class="wpcp-row">
			<label>Email Message:</label><br />
			<textarea name="coupon-em-msg"><?php echo $coupon[0]['email_msg'] ?></textarea>
		</div>
		<script type="text/javascript">
		function delete_confirm() {
			var r=confirm("Are you sure you want to delete ths coupon?")
			if (r==true) {
				window.location = "?page=<?php echo WP_COUPON_ADMIN_SLUG; ?>&coupon=<?php echo $couponid; ?>&delete";
			}
		}
		</script>
		<input id="" name="submit" type="submit" value="Save Coupon" class="button-primary" />&nbsp;&nbsp;&nbsp;<a href="?page=<?php echo WP_COUPON_ADMIN_SLUG; ?>" class="wpcp-link">Cancel</a><a href="#" class="wpcp-del-link"  onclick="delete_confirm();">Delete Coupon</a>
		</form>
	</div>
	<br /><br />
	<style>
	th.center, td.center{text-align:center;}
	td a img{vertical-align:middle;margin-right:5px;}
	</style>
	<div class="wrap">
		<a href="<?php echo $_SERVER['REQUEST_URI'] ?>&export" target="_blank" class="button-primary" style="float:right;">Export to CSV</a>
		<h3 style="margin-top:5px;">Email Entries for this Coupon</h3>
		<table class="widefat">
			<thead>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Carrier</th>
					<th>Date Entered</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$entries = $wpdb->get_results( "SELECT * FROM $wp_entry_db WHERE coupon_id = $couponid ORDER BY stamp DESC", ARRAY_A);
				if(count($entries) > 0) {
				foreach($entries as $entry) {
				?>
				<tr>
					<td><?php echo $entry['fname']; ?></td>
					<td><?php echo $entry['lname']; ?></td>
					<td><?php echo $entry['email']; ?></td>
					<td><?php echo $entry['phone']; ?></td>
					<td><?php echo $entry['carrier']; ?></td>
					<td><?php echo $entry['stamp']; ?></td>
				</tr>
				<?php }
				} else { ?>
				<tr>
					<td colspan="7" style="text-align:center;">No Entries Found</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<?php
		} else { ?>
	<p>Error: Coupon not found.</p>
	<?php 
		}
}

function wp_coupon_create_coupon() {
	global $wpdb;
	$wpcoupondb = WP_COUPON_DB_NAME;

	if(isset($_POST['submit'])) {
			$wpdb->insert(
				$wpcoupondb, 
				array(
					'stamp' => date("Y-m-d H:i:s"),
					'name' => $_POST['coupon-name'],
					'advertiser' => $_POST['coupon-advertiser'],
					'code' => $_POST['coupon-code'],
					'available' => $_POST['coupon-available'],
					'method' => $_POST['coupon-method'],
					'txt_msg' => $_POST['coupon-txt-msg'],
					'txt_from' => $_POST['coupon-txt-from'],
					'email_to' => $_POST['coupon-em-to'],
					'email_subject' => $_POST['coupon-em-subject'],
					'email_msg' => $_POST['coupon-em-msg'],
					'email_from' => $_POST['coupon-em-from']
				)
			);

			?>
			<script type="text/javascript">window.location = "?page=<?php echo WP_COUPON_ADMIN_SLUG; ?>";</script>
			<?php
			exit();
		}
	?>
	<style type="text/css">
	.wpcp-form{background-color:#eee;padding:20px;margin-right:20px;}
	.wpcp-row{display:block;margin:20px 0;border-bottom:1px solid #e0e0e0;}
	.wpcp-link{}
	.wpcp-del-link{color:#f00;text-decoration:underline;float:right;}
	.wpcp-row input, .wpcp-row select{margin-bottom:20px;}
	</style>
	<h2>New Coupon</h2>
	<p><a href="?page=<?php echo WP_COUPON_ADMIN_SLUG; ?>">Back to Admin</a></p>
	<div class="wpcp-form">
		<h3>General Settings</h3>
		<form name="" id="" action="" method="post">
		<div class="wpcp-row">
			<label>Coupon Name:</label><br />
			<input type="text" name="coupon-name" value="" />
		</div>
		<div class="wpcp-row">
			<label>Advertiser:</label><br />
			<input type="text" name="coupon-advertiser" value="" />
		</div>
		<div class="wpcp-row">
			<label>Coupon Code:</label><br />
			<input type="text" name="coupon-code" value="" />
		</div>
		<div class="wpcp-row">
			<label>Total Coupons to Win:</label><br />
			<input type="text" name="coupon-available" value="" />
		</div>
		<div class="wpcp-row">
			<label>Winning Method:</label><br />
			<select name="coupon-method">
				<option value="First to visit">First to visit</option>
				<option value="Random">Random</option>
				<option value="Everyone wins">Everyone Wins (overrides total coupons)</option>
			</select>
		</div>
		<h3>Contact Settings</h3>
		<div class="wpcp-row">
			<label>Text Message:</label><br />
			<textarea name="coupon-txt-msg"></textarea>
		</div>
		<div class="wpcp-row">
			<label>Text From (your email address):</label><br />
			<input type="text" name="coupon-txt-from" />
		</div>
		<div class="wpcp-row">
			<label>Email To (email address of advertiser):</label><br />
			<input type="text" name="coupon-em-to" />
		</div>
		<div class="wpcp-row">
			<label>Email Subject:</label><br />
			<input type="text" name="coupon-em-subject" />
		</div>
		<div class="wpcp-row">
			<label>Email From (your email address):</label><br />
			<input type="text" name="coupon-em-from" />
		</div>
		<div class="wpcp-row">
			<label>Email Message:</label><br />
			<textarea name="coupon-em-msg"></textarea>
		</div>
		<input id="" name="submit" type="submit" value="Save Coupon" class="button-primary" />&nbsp;&nbsp;&nbsp;<a href="?page=<?php echo WP_COUPON_ADMIN_SLUG; ?>" class="wpcp-link">Cancel</a>
		</form>
	</div>
	<?php
}

function wp_coupon_display_help() {
	?>
	<h2>Getting Started Guide - WP Coupon</h2>
	<p><a href="?page=<?php echo WP_COUPON_ADMIN_SLUG; ?>">Back to Admin</a></p>
	<h3>Creating, Editing, and Deleting a Coupon</h3>
	<p>To begin creating a new coupon, click the "Create New Coupon" button on the main admin page.  This will take you to a form page where you can fill out and configure your coupon settings.  Here is a quick breakdown of each field:</p>
	<h4>Coupon Name</h4>
	<p>The Coupon Name field is just a name of your choosing to help you identify the coupon.</p>
	<h4>Advertiser</h4>
	<p>The Advertiser field is generally the name of the company for which the coupon is being generated.</p>
	<h4>Coupon Code</h4>
	<p>The Coupon Code is the actual code that users will receive and use to get their discounts from your respective advertisers.</p>
	<h4>Total Coupons to Win</h4>
	<p>The Total Coupons to Win field is a numerical value representing how many coupons you'd like to make available for redemption off your site.</p>
	<h4>Winning Method</h4>
	<p>The Winning Method field determines how the coupons are distributed and won.  Here is how the winning methods generally work:</p>
	<p>
		<blockquote>
			<h5>Winning Methods Breakdown</h5>
			<ul>
				<li>
					<b>First to win:</b>
					<p>This method displays the respective coupon to the user and renders the content page in which it was used based on the first users to the page.  Once the Total Coupons limit is reached, the coupon will no longer display, and instead the user will be directed to the losing/other page.</p>
				</li>
				<li>
					<b>Random:</b>
					<p>This method behaves the same way as the First to Win method in that it will continue to display the coupon until the Total Coupons limit is reached, except that it does so in a random manner.  So until the limit is reached, users will randomly view the coupon page or be sent to the losing page.  This random behavior will continue until the Total Coupons limit is reached.</p>
				</li>
				<li>
					<b>Everyone wins:</b>
					<p>This method will always display the coupon.</p>
				</li>
			</ul>
		</blockquote>
	</p>
	<h3>Using a Coupon</h3>
	<p>To use a coupon, you need two bits of information.  You need the "losing URL" (full url path, i.e. http://www.yahoo.com), which is where users are sent to if they do not "win" the chance to view the coupon.  The other piece of information that you will need is the coupon ID.  You can get the coupon ID from the main admin page where all your coupons are listed.  You will see the coupon ID as the last column in the table.</p>

	<p>Once you have gathered the losing url and the coupon ID, you can now use the coupon on any content page you create in Wordpress.  You can use the coupon by using the WP Coupon "shortcut code".  The shortcut code will return the actual coupon code or it will redirect the user to the losing url.  So basically, you create your winning page, and slap this shortcut code where you want the actual code to display on the page, and the plugin will handle the rest by either displaying the coupon code on your page, or redirecting the user to your specified losing URL.</p>

	<p>Here is an example of how to use the shortcut code:</p>

	<p>[get_coupon losing_url="http://www.mysite.com/maybe-next-time/" coupon_id="3"]</p>

	<p>In the above example, you would add this code right on your winning page where you would like the coupon code to display.</p>
	<?php
}

/** Step 3. */
function wp_coupon_render_page() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	if(isset($_REQUEST['coupon'])) {
		wp_coupon_update_coupon();
	} elseif(isset($_REQUEST['new-coupon'])) {
		wp_coupon_create_coupon();
	} elseif(isset($_REQUEST['section'])) {
		switch($_REQUEST['section']) {
			case 'getting-started':
			wp_coupon_display_help();
			break;
		}
	} else {
		wp_coupon_display_main_admin();
	}

	wp_coupon_display_footer();
}
