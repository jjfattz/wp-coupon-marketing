<?php
/**
 * Send SMS via Email to SMS gateways
 * 
 * Please note that the list of carreirs is UTF-8 encoded. ie: Mexico != México
 * 
 * @author gabe@fijiwebdesign.com
 * @link http://www.fijiwebdesign.com/
 */
class email_to_sms {
	/**
	 * A list of the major Phone carriers and their Email 2 SMS gateway syntax
	 * Compiled from: http://en.wikipedia.org/wiki/List_of_carriers_providing_SMS_transit
	 */
	protected static $carriers = array(
		"Airtel" => array("Andhra Pradesh, India", "#@airtelap.com"),
		"Airtel" => array("Karnataka, India", "#@airtelkkk.com"),
		"Alaska Communications Systems" => array("USA", "#@msg.acsalaska.net"),
		"Alltel Wireless Verizon Wireless" => array("USA", "#@text.wireless.alltel.com,#@mms.alltel.net"),
		"aql" => array("UK", "#@text.aql.com"),
		"AT&T" => array("USA", "#@txt.att.net,#@mms.att.net"),
		"Bell" => array("Canada", "#@txt.bell.ca,#@txt.bellmobility.ca"),
		"Boost Mobile" => array("USA", "#@boostmobile.com"),
		"Bouygues Télécom (company)" => array("France", "#@mms.bouyguestelecom.fr"),
		"[[Loop_Mobile_India|Loop] (BPL Mobile)" => array("Mumbai, India", "#@bplmobile.com"),
		"Cellular One" => array("USA", "#@mobile.celloneusa.com"),
		"Cingular" => array("USA", "#@cingular.com"),
		"Centennial Wireless" => array("United States, Puerto Rico, U.S. Virgin Islands", "#@cwemail.com"),
		"Cincinnati Bell" => array("Cincinnati, Ohio, USA", "#@gocbw.com,#@mms.gocbw.com"),
		"Claro" => array("Brasil", "#@clarotorpedo.com.br"),
		"Comcel" => array("Colombia", "#@comcel.com.co"),
		"Cricket" => array("", "#@mms.mycricket.com,#@sms.mycricket.com"),
		"CTI Móvil Claro" => array("Argentina", "#@sms.ctimovil.com.ar"),
		"Emtel" => array("Mauritius", "#@emtelworld.net"),
		"Fido" => array("Canada", "#@fido.ca"),
		"General Communications Inc." => array("Alaska", "#@msg.gci.net"),
		"Globalstar satellite" => array("", "#@msg.globalstarusa.com"),
		"Helio" => array("", "#@myhelio.com"),
		"Iridium" => array("", "#@msg.iridium.com"),
		"i-wireless (Sprint PCS)" => array("", "#@iwirelesshometext.com"),
		"Mero Mobile" => array("Nepal", "#@sms.spicenepal.com"),
		"MetroPCS" => array("", "#@mymetropcs.com"),
		"Movicom" => array("", "#@movimensaje.com.ar"),
		"Mobitel" => array("Sri Lanka", "#@sms.mobitel.lk"),
		"Movistar" => array("Colombia", "#@movistar.com.co"),
		"MTN" => array("South Africa", "#@sms.co.za"),
		"MTS Mobility" => array("Canada", "#@text.mtsmobility.com"),
		"Nextel" => array("United States", "#@messaging.nextel.com"),
		"Nextel" => array("México", "#@msgnextel.com.mx"),
		"Nextel" => array("Argentina", "#@nextel.net.ar"),
		"Orange Polska" => array("Poland", "#@orange.pl"),
		"Personal" => array("Argentina", "#@alertas.personal.com.ar"),
		"Plus" => array("Poland", "#@text.plusgsm.pl"),
		"PC Telecom" => array("Canada", "#@mobiletxt.ca"),
		"Qwest Wireless" => array("USA", "#@qwestmp.com"),
		"Rogers Wireless" => array("Canada", "#@pcs.rogers.com"),
		"SaskTel" => array("Canada", "#@sms.sasktel.com"),
		"Setar" => array("Aruba", "#@mas.aw"),
		"Sprint (PCS)" => array("USA", "#@messaging.sprintpcs.com,#@pm.sprint.com"),
		"Sprint (Nextel)" => array("USA", "#@page.nextel.com,#@messaging.nextel.com"),
		"Suncom" => array("", "#@tms.suncom.com"),
		"Sunrise Communications" => array("Switzerland", "#@gsm.sunrise.ch"),
		"Syringa Wireless" => array("USA", "#@rinasms.com"),
		"T-Mobile" => array("USA", "#@tmomail.net"),
		"T-Mobile" => array("Austria", "#@sms.t-mobile.at"),
		"T-Mobile" => array("Croatia", "#@sms.t-mobile.hr"),
		"Telus Mobility" => array("Canada", "#@msg.telus.com"),
		"Tigo" => array("Colombia", "#@sms.tigo.com.co"),
		"Tracfone" => array("", "#@mmst5.tracfone.com,#@txt.att.net,#@tmomail.net,#@vtext.com,#@email.uscc.net,#@message.alltel.com"),
		"Unicel" => array("USA", "#@utext.com"),
		"US Cellular" => array("USA", "#@email.uscc.net,#@mms.uscc.net"),
		"Verizon" => array("USA", "#@vtext.com,#@vzwpix.com"),
		"Viaero" => array("USA", "#@viaerosms.com,#@mmsviaero.com"),
		"Vivo" => array("Brasil", "#@torpedoemail.com.br"),
		"Virgin Mobile" => array("Canada", "#@vmobile.ca"),
		"Virgin Mobile" => array("USA", "#@bills.com,#@vmobl.com,#@vmpix.com"),
		"Vodacom" => array("South Africa", "#@voda.co.za")
	);
	
	/**
	 * Send an SMS to a Phone Number
	 * @return Bool
	 * @param $msg String SMS/TXT message
	 * @param $to Int Receivers Phone number
	 * @param $from String From Email address or phone number
	 * @param $carrier String Carrier Name (index of self::$carriers)
	 * 
	 * @todo 	at the moment we only send to the first listed email
	 * 			In the future it would be good to check the existence of mailbox at each domain
	 * 
	 * 
	 */
	public function send($msg, $to, $carrier, $from) {
		$carriers = self::$carriers;
		if (!isset($carriers[$carrier])) {
			//throw new Exception('The requested carrier was not found');
			var_dump($carriers[0][$carrier]);echo '<br />';
			var_dump($carrier);echo '<br />';
			var_dump($carriers);
			return false;
		}
		$addresses = explode(',', $carriers[$carrier][1]);
		$address = str_replace('#', $to, $addresses[0]);
		/*
		// using PHP mail() function
		if (!mail($address, $msg, $msg, "from:$from\n")) {
			throw new Exception('The Email could not be sent to SMS gateway');
			return false;
		}*/

		// using Wordpress wp-mail() function
		if(!wp_mail( $address, $msg, $msg, "from:$from\n" )) {
			throw new Exception('The Email could not be sent to SMS gateway');
			return false;
		}

		return true;
	}
	
	/**
	 * Return a Carrier by phone number
	 * @return Array
	 * @param $name String
	 */
	public function getCarrierByNumber($number) {
		// todo
		// here we would query MTA at each domain for mailbox existence
	}
	
	/**
	 * Return a Carrier by name
	 * @return Array
	 * @param $name String
	 */
	public function getCarrierByName($name) {
		return isset(self::$carriers[$name]) ? self::$carriers[$name] : null;
	}
	
	/**
	 * Retrieve a list of Carriers by region
	 * @return Array
	 * @param $region String[optional] Return only gateways in this region. eg: "USA" or "India"
	 */
	public function getCarriersByRegion($region = false) {
		$carriers = self::$carriers;
		if ($region) {
			$_carriers = array();
			$region = strtolower($region);
			foreach($carriers as $carrier) {
				if (in_array($region, explode(', ', strtolower($carrier[1])))) {
					$_carriers[] = $carrier;
				}
			}
			return $_carriers;
		}
		return $carriers;
	}
	
	/**
	 * Retrieve the regions in carrier list
	 * @return Array
	 */
	public function getRegions() {
		$regions = array();
		foreach(self::$carriers as $carrier) {
			$regions[$carrier->region] = 1;
		}
		return array_keys($regions);
	}
	
}
?>